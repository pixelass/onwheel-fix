import browserDetector from 'detect-browser'

const browserName = browserDetector && browserDetector.name
const browserVersion = browserDetector && browserDetector.version

function majorVersion () {
  if (!browserVersion) {
    return null
  }

  let majorVersionString = browserVersion.split('.')[0]
  return parseInt(majorVersionString, 10)
}

function isSafari () {
  if (!browserName) {
    return false
  }

  return browserName === 'safari'
}

function isSafari9 () {
  return isSafari() && majorVersion() === 9
}

function isSafari10 () {
  return isSafari() && majorVersion() === 10
}

exports.hasBadMouseBehavior = () => {
  return isSafari9() || isSafari10()
}
